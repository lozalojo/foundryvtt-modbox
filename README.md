# ModBox
This FoundryVTT Module will add an input box to the toolbar to modify the next roll

## Setup
To install this module, go to the World Configuration and Setup, Addon Modules, Install Module.
Then you may copy this url https://gitlab.com/mesfoliesludiques/foundryvtt-modbox/-/raw/master/module.json
In the Module Settings you can specify if you want to reset the modifier value after usage (default to true);

## Contributions
Every contribution is welcome.